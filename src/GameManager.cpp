#include "../include/GameManager.hpp"
#include <ctime>

GameManager::GameManager()
{
}

GameManager::~GameManager()
{
}

void GameManager::privateInit()
{
  // Set default OpenGL states
  glEnable(GL_CULL_FACE);

  // Adding the camera to the scene
  cam_.reset(new Camera());
  this->addSubObject(cam_);

  skybox_.reset(new SkyBox());
  this->addSubObject(skybox_);

  ///MOVE WATER AWAY
  //  bf_.reset(new BattleField());
  //  this->addSubObject(bf_);
  //  bf_->setOffset(offset);

  //  bf2_.reset(new BattleField());
  //  this->addSubObject(bf2_);
  //  bf2_->setOffset(offset);

  spaceship_.reset(new SpaceShip());
  this->addSubObject(spaceship_);

  timer_.start();
  shootTimer_.start();
  isGameActive = true;
  terrainFlag = true, terrain2Flag = true;
  wave = 0;

  glDisable(GL_CULL_FACE);
}

void GameManager::privateRender()
{
  // Nothing to render
  textCharacter.drawText("This is America", 25.0f, 25.0f, 1.0f);
}

void GameManager::privateUpdate()
{
  //Set Camera Matrix
  this->matrix_ = cam_->getMatrix();

  //Allign Skybox Positon to Camera's
  skybox_->setPos(glm::vec3(-matrix_[3][0], -matrix_[3][1], -matrix_[3][2]));

  if(isGameActive){
    //1. Spawn Terrain
    spawnTerrains();

    //2. Spawn Enemies
    if (timer_.elapsed() >= 5.0){
      spawnEnemyShips();
      timer_.reset();
      timer_.start();
    }

    //3. Spawn Enemies' bullets
    enemyShoot();

    //4. Check out of Bounds condition
    checkOutOfBounds();
    setBoundaryToTerrain();

    //5. Check Collisions
    collisionDetections();

    removeParticles();

  }
}

void GameManager::spawnTerrains()
{
  if (terrainFlag){
    bf_.reset(new BattleField());
    bf_->setOffset(offset1);
    this->addSubObject(bf_);
    bf_->init();
    offset1+=2.0f;
    terrainFlag = false;
  }

  if (terrain2Flag){
    bf2_.reset(new BattleField());
    bf2_->setOffset(offset2);
    this->addSubObject(bf2_);
    bf2_->init();
    offset2+=2.0f;
    terrain2Flag = false;
    std::cout << "flag: " << terrainFlag << std::endl;
  }
}

void GameManager::spawnEnemyShips()
{
  wave++;
  if (wave==5){
    level++;
    spaceship_->gainHealth(20);
    wave = 0;
  }

  srand(std::time(0));
  for (unsigned int i=0; i < wave; ++i){
    int randomNumber = rand() % (int)(wave);
    enemyship_.reset(new EnemyShip());
    zPlace = cam_->getMatrix()[3].z + (50*i) + 500.0f;
    enemyship_->setZPlace(-zPlace);
    enemyship_->setXPlace(-bf_->getHalfSize()+10 + randomNumber*10);
    this->addSubObject(enemyship_);
    enemies_.push_back(enemyship_);
    enemyship_->init();
  }
}

void GameManager::enemyShoot()
{
  if(!this->enemies_.empty()){
    for (auto enemy : enemies_) {
      if (enemy->getReload()){
        bullet_.reset(new Bullets());
        bullet_->setStartPos((enemy->getMatrix()));
        glm::vec3 dir = glm::normalize(spaceship_->getMatrix()[3] - enemy->getMatrix()[3]);
        bullet_->setDir(dir);
        bullet_->setType(0);
        this->addSubObject(bullet_);
        enemyBullets_.push_back(bullet_);
        bullet_->init();
      }
    }
  }
}

void GameManager::checkOutOfBounds()
{
  //Check If Enemiees Out of Bounds
  if(!this->enemies_.empty()){
    for(auto enemy_it = enemies_.begin(); enemy_it != enemies_.end();) {
      auto enemy = *(enemy_it);
      if(enemy->getMatrix()[3][2] >= spaceship_->getMatrix()[3][2]){
        this->removeSubObject(enemy);
        enemy_it = this->enemies_.erase(enemy_it);
        this->spaceship_->losePoints(20);
      }
      else
        enemy_it++;
    }
  }
  //Check If Enemies' Bullets are Out of Bounds
  if(!this->enemyBullets_.empty()){
    for(auto bullet_it = enemyBullets_.begin(); bullet_it != enemyBullets_.end();) {
      auto bullet = *(bullet_it);
      if(bullet->getMatrix()[3][2] >= -cam_->getMatrix()[3][2]){
        this->removeSubObject(bullet);
        bullet_it = enemyBullets_.erase(bullet_it);
      }
      else
        bullet_it++;
    }
  }
  //Check If Player's Bullets Out of Bounds
  if(!this->playerBullets_.empty()){
    for(auto bullet_it = playerBullets_.begin(); bullet_it != playerBullets_.end();) {
      auto bullet = *(bullet_it);
      if(bullet->getMatrix()[3][2] < -cam_->getMatrix()[3].z - 500.0f){
        this->removeSubObject(bullet);
        bullet_it = playerBullets_.erase(bullet_it);
      }
      else
        bullet_it++;
    }
  }
  //Check if terrain's are out of bounds
  if(bf_!=nullptr){
    if(bf_->getMatrix()[3][2] > -cam_->getMatrix()[3][2]){
      this->removeSubObject(bf_);
      std::cout << "Terrain 1 removed" << std::endl;
      terrainFlag = true;
    }
  }
  if(bf2_!=nullptr){
    if(bf2_->getMatrix()[3][2] > -cam_->getMatrix()[3][2]){
      this->removeSubObject(bf2_);
      std::cout << "Terrain 2 removed" << std::endl;
      terrain2Flag = true;
    }
  }
}

void GameManager::setBoundaryToTerrain()
{
  if(bf_!=nullptr || bf2_!=nullptr){
    if((spaceship_->getMatrix()[3][0] < -bf_->getHalfSize()+10)||
       (spaceship_->getMatrix()[3][0] < -bf2_->getHalfSize()+10)){
      spaceship_->setLeftBoundary(false);
    }
    else if((spaceship_->getMatrix()[3][0] > bf_->getHalfSize()-10)||
            (spaceship_->getMatrix()[3][0] > bf_->getHalfSize()-10)){
      spaceship_->setRightBoundary(false);
    }
    else {
      spaceship_->setLeftBoundary(true);
      spaceship_->setRightBoundary(true);
    }
    if(!this->enemies_.empty()){
      for(auto enemy_it = enemies_.begin(); enemy_it != enemies_.end();) {
        auto enemy = *(enemy_it);
        if((enemy->getMatrix()[3][0] > bf_->getHalfSize()-10)||
           (enemy->getMatrix()[3][0] > bf_->getHalfSize()-10)){
          enemy->setLeftBoundary(false);
        }
        else if((enemy->getMatrix()[3][0] < -bf_->getHalfSize()+10)||
                (enemy->getMatrix()[3][0] < -bf_->getHalfSize()+10)){
          enemy->setRightBoundary(false);
        }
        else {
          enemy->setLeftBoundary(true);
          enemy->setRightBoundary(true);
        }
        enemy_it++;
      }
    }
  }
}

void GameManager::collisionDetections(){
  if(!this->enemies_.empty()){
    this->checkEnemyPlayerHit();
    if(!this->playerBullets_.empty())
      this->checkBulletEnemyHit();

    if(!this->enemyBullets_.empty())
      this->checkBulletPlayerHit();

    if((!this->enemyBullets_.empty()) &&
       (!this->playerBullets_.empty()))
      this->checkBulletBulletHit();
  }
}

void GameManager::checkBulletEnemyHit()
{
  bool hit = false;
  for(auto bullet_it = this->playerBullets_.begin(); bullet_it != this->playerBullets_.end();){
    auto bullet = *(bullet_it);
    for(auto enemy_it = this->enemies_.begin(); enemy_it != this->enemies_.end(); ++enemy_it){
      auto enemy = *(enemy_it);
      if(checkBulletEnemyCollision(enemy,bullet)){
        enemy->damaged(bullet->getImpact());
        this->removeSubObject(bullet);
        bullet_it = playerBullets_.erase(bullet_it);

        std::cout << "Enemy LIFE: " << enemy->getLife() << std::endl;

        if (enemy->getLife() <= 0){
          std::cout << "enemy dead" << std::endl;
          particle_.reset(new TestEnvironment());
          particle_->setMatrix(enemy->getMatrix());
          this->addSubObject(particle_);
          particles_.push_back(particle_);
          particle_->init();

          this->removeSubObject(enemy);
          enemy_it = enemies_.erase(enemy_it);

          spaceship_->setPoints(level * 50);
          if (highScore <= spaceship_->getPoint())
            highScore = spaceship_->getPoint();
        }
        hit = true;
        break;
      }
    }
    if (!hit) bullet_it++; else (hit = false);
  }
}

void GameManager::checkEnemyPlayerHit()
{
  for(auto enemy_it = this->enemies_.begin(); enemy_it != this->enemies_.end();) {

    auto enemy = *(enemy_it);
    if(checkEnemyPlayerCollision(enemy)){
      std::cout << "boom!!" << std::endl;
      this->spaceship_->damaged(enemy->getImpact());
      this->removeSubObject(enemy);
      enemy_it = this->enemies_.erase(enemy_it);
      //PARTICLES();
      std::cout << "SPACESHIP LIFE: " << spaceship_->getLife() << std::endl;
      std::cout << "SPACESHIP ARMOUR: " << spaceship_->getArmor() << std::endl;

      if (this->spaceship_->getLife() <= 0){
        std::cout << "dead" << std::endl;
        this->removeSubObject(this->spaceship_);
        isGameActive = false;
        //END GAME()
      }
    }
    else
      enemy_it++;
  }
}

void GameManager::checkBulletPlayerHit()
{
  for(auto bullet_it = this->enemyBullets_.begin(); bullet_it != this->enemyBullets_.end();) {
    auto bullet = *(bullet_it);
    if(checkBulletPlayerCollision(bullet)){
      std::cout << "boom!!" << std::endl;
      this->spaceship_->damaged(bullet->getImpact());
      this->removeSubObject(bullet);
      bullet_it = this->enemyBullets_.erase(bullet_it);
      //PARTICLES();
      std::cout << "SPACESHIP LIFE: " << spaceship_->getLife() << std::endl;
      std::cout << "SPACESHIP ARMOUR: " << spaceship_->getArmor() << std::endl;

      if (this->spaceship_->getLife() <= 0){
        std::cout << "dead" << std::endl;
        this->removeSubObject(this->spaceship_);
        isGameActive = false;
        //END GAME()
      }
    }
    else
      bullet_it++;
  }
}

void GameManager::checkBulletBulletHit()
{
  bool hit = false;
  for(auto pBullet_it = this->playerBullets_.begin(); pBullet_it != this->playerBullets_.end();){
    auto pBullet = *(pBullet_it);
    for(auto eBullet_it = this->enemyBullets_.begin(); eBullet_it != this->enemyBullets_.end(); ){
      auto eBullet = *(eBullet_it);
      if(checkBulletBulletCollision(eBullet,pBullet)){
        std::cout << "tikk!!" << std::endl;
        this->removeSubObject(eBullet);
        eBullet_it = enemyBullets_.erase(eBullet_it);
        this->removeSubObject(pBullet);
        pBullet_it = playerBullets_.erase(pBullet_it);
        //PARTICLES() small;

        hit = true;
        break;
      }
      else eBullet_it++;
    }
    if (!hit) {
      pBullet_it++;
    }
    else (hit = false);
  }
}

bool GameManager::checkBulletPlayerCollision(std::shared_ptr<Bullets> bullet)
{
  float bullet_radius = bullet->getHalfSize();
  glm::mat4 bullet_pos = bullet->getMatrix();
  float spaceship_radius = spaceship_->getHalfSize();
  glm::mat4 spaceship_pos = spaceship_->getMatrix();

  glm::vec3 bullet_center(bullet_pos[3] + bullet_radius);
  glm::vec3 spaceship_center(spaceship_pos[3] + spaceship_radius);

  glm::vec3 difference = bullet_center - spaceship_center;
  if (glm::length(difference) <= bullet_radius+spaceship_radius)
    return true;
  else
    return false;
}

bool GameManager::checkEnemyPlayerCollision(std::shared_ptr<EnemyShip> enemy)
{
  float enemy_radius = enemy->getHalfSize();
  glm::mat4 enemy_pos = enemy->getMatrix();
  float spaceship_radius = spaceship_->getHalfSize();
  glm::mat4 spaceship_pos = spaceship_->getMatrix();

  glm::vec3 enemy_center(enemy_pos[3] + enemy_radius);
  glm::vec3 spaceship_center(spaceship_pos[3] + spaceship_radius);

  glm::vec3 difference = enemy_center - spaceship_center;
  if (glm::length(difference) <= enemy_radius+spaceship_radius)
    return true;
  else
    return false;
}

bool GameManager::checkBulletEnemyCollision(std::shared_ptr<EnemyShip> enemy, std::shared_ptr<Bullets> bullet)
{
  float enemy_radius = enemy->getHalfSize();
  glm::mat4 enemy_pos = enemy->getMatrix();
  float bullet_radius = bullet->getHalfSize();
  glm::mat4 bullet_pos = bullet->getMatrix();

  glm::vec3 enemy_center(enemy_pos[3] + enemy_radius);
  glm::vec3 bullet_center(bullet_pos[3] + bullet_radius);

  glm::vec3 difference = enemy_center - bullet_center;
  if (glm::length(difference) <= enemy_radius+bullet_radius)
    return true;
  else
    return false;
}

bool GameManager::checkBulletBulletCollision(std::shared_ptr<Bullets> enemyBullet, std::shared_ptr<Bullets> playerBullet)
{
  float enemyBullet_radius = enemyBullet->getHalfSize();
  glm::mat4 enemyBullet_pos = enemyBullet->getMatrix();
  float playerBullet_radius = playerBullet->getHalfSize();
  glm::mat4 playerBullet_pos = playerBullet->getMatrix();

  glm::vec3 enemyBullet_center(enemyBullet_pos[3] + enemyBullet_radius);
  glm::vec3 playerBullet_center(playerBullet_pos[3] + playerBullet_radius);

  glm::vec3 difference = enemyBullet_center - playerBullet_center;
  if (glm::length(difference) <= enemyBullet_radius+playerBullet_radius)
    return true;
  else
    return false;
}
void GameManager::toggleWeapon()
{

}

void GameManager::removeParticles()
{
  for (auto it = particles_.begin(); it != particles_.end();) {
    if ((*it)->faded) {
      removeSubObject(*it);
      it = particles_.erase(it);
    }
    else
      ++it;
  }
}

void GameManager::playerShoot()
{
  if(spaceship_->getReload()){
    bullet_.reset(new Bullets());
    bullet_->setStartPos((spaceship_->getMatrix()));
    glm::vec3 mousey = glm::normalize(glm::vec3(-spaceship_->getSpin(), 0.0f, 60.0f));
    bullet_->setDir(-mousey);
    bullet_->setType(1);
    this->addSubObject(bullet_);
    playerBullets_.push_back(bullet_);
    bullet_->init();
  }
}

std::shared_ptr<Camera> GameManager::getCam()
{
  return cam_;
}

std::shared_ptr<SpaceShip> GameManager::getSpaceShip()
{
  return spaceship_;
}

std::shared_ptr<Bullets> GameManager::getBullet()
{
  return bullet_;
}
