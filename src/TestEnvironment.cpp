
#include "../include/TestEnvironment.hpp"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "../include/SpaceShip.hpp"
#include "../include/EnemyShip.hpp"

TestEnvironment::TestEnvironment()
{
}

TestEnvironment::~TestEnvironment()
{
}


void TestEnvironment::privateInit()
{
  textCharacter.initText(glm::vec3(0.5f,0.8f, 0.2f));

  for (auto i = 0; i < MAX_PARTICLES; ++i) {
    particles_[i].active = true;
    particles_[i].life = 1.0f;
    particles_[i].fade = float(rand() % 100) / 1000.0f + 0.003f;
    particles_[i].xi = float((rand() % 50) - 26.0f) * 25.0f;
    particles_[i].yi = float((rand() % 50) - 25.0f) * 25.0f;
    particles_[i].zi = float((rand() % 50) - 25.0f) * 25.0f;
    particles_[i].xg = 0.0f;
    particles_[i].yg = -0.941f;
    particles_[i].zg = 0.0f;
    particles_[i].x = matrix_[3].x;
    particles_[i].y = matrix_[3].y;
    particles_[i].z = matrix_[3].z;
  }

  particleTexture.init2DTextures("D:/slimk/Documents/VR/SpaceShooters/resources/particle.bmp", 0);
  particleShader.initShaders("D:/slimk/Documents/VR/SpaceShooters/shaders/brick");

}

void TestEnvironment::privateRender()
{
  textCharacter.drawText("This is America", -10.0f, 25.0f, 0.0f);

  prepareRender();

  for (auto i = 0; i < MAX_PARTICLES; i++) {
    if (particles_[i].active) {
      float x = particles_[i].x;
      float y = particles_[i].y;
      float z = particles_[i].z;

      glBegin(GL_TRIANGLES);
      glTexCoord2d(1, 1);
      glVertex3f(x + 0.5f, y + 0.5f, z);
      glTexCoord2d(0, 1);
      glVertex3f(x - 0.5f, y + 0.5f, z);
      glTexCoord2d(1, 0);
      glVertex3f(x + 0.5f, y - 0.5f, z);
      glTexCoord2d(0, 0);
      glVertex3f(x - 0.5f, y - 0.5f, z);
      glEnd();

      particles_[i].x += particles_[i].xi / _slowdown;
      particles_[i].y += particles_[i].yi / _slowdown;
      particles_[i].z += particles_[i].zi / _slowdown;

      particles_[i].xi += particles_[i].xg;
      particles_[i].yi += particles_[i].yg;
      particles_[i].zi += particles_[i].zg;

      particles_[i].life -= 1/fps_;

      particleShader.enable();
      glUniform1f(glGetUniformLocation(particleShader.getProg(), "fade"), particles_[i].life);

      if (particles_[i].life < 0.0f) {
        faded = true;
      }

      glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
      glEnd();
    }
  }

  disableRender();
}

void TestEnvironment::privateUpdate()
{
  
}

void TestEnvironment::prepareRender()
{
  //Enable Shader
  particleShader.enable();

  //Enable Texture
  glShadeModel(GL_SMOOTH);
  glDisable(GL_DEPTH_TEST);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE);
  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
  glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
  particleTexture.bind2D(0);
}

void TestEnvironment::disableRender()
{
  //Disable Texture
  particleTexture.unBind2D(0);
  glDisable(GL_BLEND);
  glEnable(GL_DEPTH_TEST);

  //Disable Shader
  particleShader.disable();
}
