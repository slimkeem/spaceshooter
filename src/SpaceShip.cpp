#include "../include/SpaceShip.hpp"
#include <glm/gtc/matrix_transform.hpp>

#include <iostream>


SpaceShip::SpaceShip()
{
  //Start timer
  timer.start();

  //Set initial position of object
  matrix_ = glm::mat4(1.0);
  //  matrix_ = glm::rotate(matrix_, glm::radians(10.0f), glm::vec3(0.0, 1.0, 0.0));

  //Set some properties
  armor_ = 0;
  life_ = 100;
  spin_ = 0.0f;
  speed_ = 0.01f;
  size_ = 1.0f;
  point_ = 0.0f;
}

SpaceShip::~SpaceShip()
{
}

void SpaceShip::privateInit()
{
  matrix_ = glm::translate(matrix_, glm::vec3(0.0f, 0.0f, -10.0f));
  //Initialize Shader
  shipShader_.initShaders("D:/slimk/Documents/VR/SpaceShooters/shaders/phong");


  //Initialize Texture
  texture_.init2DTextures("D:/slimk/Documents/VR/SpaceShooters/resources/planet_texture.png", 0);


  //Initialize Mesh
  planet_.loadModel("D:/slimk/Documents/VR/SpaceShooters/resources/SpaceShip.obj");
  //  planet_.loadModel("D:/slimk/Documents/VR/SpaceShooters/resources/Intergalactic_Spaceship-(Wavefront).obj");


  //  matrix_ = glm::scale(matrix_, glm::vec3(size_));


  //Edit Shader Uniforms
  shipShader_.enable();
  glUniform1i(glGetUniformLocation(shipShader_.getProg(), "u_Texture"), 0);
  shipShader_.disable();

}

void SpaceShip::privateRender()
{
  //Transform Object
  glRotatef(-spin_, 0.0, 1.0, 0.0);

  //Use Shaders
  shipShader_.enable();

  //Use Textures
  texture_.bind2D(0);

  //Draw
  planet_.draw(GL_TRIANGLES);

  //Disable Texture
  texture_.unBind2D(0);

  //Disable Shader
  shipShader_.disable();
}

void SpaceShip::privateUpdate()
{
  matrix_ = glm::translate(matrix_, glm::vec3(0.0f, 0.0f, -0.1));

  if (timer.elapsed() >= 0.3){
    reload_ = true;
    timer.reset();
    timer.start();
  }
  else{
    reload_ = false;
  }
}

unsigned int SpaceShip::getPoint()
{
  return point_;
}

float SpaceShip::getSpin()
{
  return spin_;
}

void SpaceShip::spin(float aimX)
{
  spin_ = aimX;
  //  if (spin_ > 360.0f)
  //    spin_ = spin_ - 360.0f;
  if(spin_ > 15.0f)
    spin_ =  15.0f;
  if(spin_ < -15.0f)
    spin_ = -15.0f;
}

void SpaceShip::damaged(int hit)
{
  while (hit > 0) {
    if (armor_ == 0){
      if (life_ == 0)
        break;
      else {
        life_ -= 1;
        --hit;
      }
    }
    else{
      armor_ -= 1;
      --hit;
    }
  }
}

void SpaceShip::gainHealth(unsigned int point)
{
  while (point > 0) {
    if (armor_ == 100){
      if (life_ == 100)
        break;
      else {
        life_ += 1;
        --point;
      }
    }
    else{
      armor_ += 1;
      --point;
    }
  }
}

void SpaceShip::losePoints(unsigned int hit)
{
  while (hit > 0) {
    if (point_ == 0)
      break;
    else {
      point_ -= 1;
      --hit;
    }
  }
}

void SpaceShip::setPoints(unsigned int point)
{
  while (point > 0) {
    point_ += 1;
    --point;
  }
}

unsigned int SpaceShip::getLife()
{
  return life_;
}

unsigned int SpaceShip::getArmor()
{
  return armor_;
}

float SpaceShip::getSpeed()
{
  return speed_;
}

float SpaceShip::getHalfSize()
{
  return size_/2;
}

bool SpaceShip::getReload()
{
  return reload_;
}

void SpaceShip::moveRight()
{
  if (rightBoundary_)
    matrix_ = glm::translate(matrix_, glm::vec3(speed_, 0.0f, 0.0f));
}

void SpaceShip::moveLeft()
{
  if (leftBoundary_)
    matrix_ = glm::translate(matrix_, glm::vec3(-speed_, 0.0f, 0.0f));
}

void SpaceShip::moveForward()
{
  matrix_ = glm::translate(matrix_, glm::vec3(0.0f, 0.0f, -speed_));
}

void SpaceShip::setLeftBoundary(bool value)
{
  leftBoundary_ = value;
}

void SpaceShip::setRightBoundary(bool value)
{
  rightBoundary_ = value;
}

