//#include "../include/Terrain.hpp"
//#include <glm/gtc/matrix_transform.hpp>

//Terrain::Terrain()
//{
//}

//Terrain::~Terrain()
//{
//}

//void Terrain::initTerrain()
//{
//  int scaleX = 1;
//  size_ = nCols*scaleX;
//  for(unsigned int z = 0; z < nRows; ++z){
//    for(unsigned int x = 0; x < nCols; ++x){
//      vertexArray_.positions.push_back(glm::vec3(x,0,z));
//      vertexArray_.texCoords.push_back(glm::vec2(float(x)/float(nCols),float(z)/float(nRows)));
//    }
//  }

//  for(unsigned int z = 0; z < (nRows-1); ++z){
//    for(unsigned int x = 0; x < (nCols-1); ++x){
//      vertexArray_.indices.push_back(z*nCols + x);
//      vertexArray_.indices.push_back(z*nCols + x+1);
//      vertexArray_.indices.push_back((z+1)*(nCols)+x+1);
//      vertexArray_.indices.push_back((z+1)*(nCols)+x);
//    }
//  }


//  //Initialize Shader
//  floorShader_.initShaders("D:/slimk/Documents/VR/SpaceShooters/shaders/terrain");


//  //Initialize Texture
//  texture_.init2DTextures("D:/slimk/Documents/VR/SpaceShooters/resources/height_map.bmp", HEIGHTMAP);
//  texture_.init2DTextures("D:/slimk/Documents/VR/SpaceShooters/resources/color_map.bmp", COLORMAP);
//  texture_.init2DTextures("D:/slimk/Documents/VR/SpaceShooters/resources/light_map.bmp", LIGHTMAP);
//  texture_.init2DTextures("D:/slimk/Documents/VR/SpaceShooters/resources/normal_map.bmp", NORMALMAP);


//  //Initialize Mesh
//  mesh_.createModel(&vertexArray_);


//  //Edit Shader Uniforms
//  floorShader_.enable();
//  glUniform1i(glGetUniformLocation(floorShader_.getProg(), "u_heightMap"), HEIGHTMAP);
//  glUniform1i(glGetUniformLocation(floorShader_.getProg(), "u_colorMap"), COLORMAP);
//  glUniform1i(glGetUniformLocation(floorShader_.getProg(), "u_lightMap"), LIGHTMAP);
//  glUniform1i(glGetUniformLocation(floorShader_.getProg(), "u_normalMap"), NORMALMAP);
//  floorShader_.disable();
//}

//void Terrain::drawTerrain()
//{
//  //Set Terrain
//  glTranslatef(nCols/-2.0f, 0.0f, nRows*-1.0f);

//  //Use Shaders
//  floorShader_.enable();

//  //Use Textures
//  texture_.bind2D(HEIGHTMAP);
//  texture_.bind2D(COLORMAP);

//  //Draw
//  mesh_.draw(GL_QUADS);

//  //Disable Texture
//  texture_.unBind2D(COLORMAP);
//  texture_.unBind2D(HEIGHTMAP);

//  //Disable Shader
//  floorShader_.disable();
//}

//float Terrain::getHalfSize()
//{
//  return size_/2;
//}
