#include "../include/SkyBox.hpp"

#include <glm/gtc/matrix_transform.hpp>


SkyBox::SkyBox()
{
}

SkyBox::~SkyBox()
{
}

void SkyBox::setPos(const glm::vec3 &vec)
{
  matrix_ = glm::mat4(1.0);
  matrix_ = glm::translate(matrix_, vec);
}


void SkyBox::privateInit()
{
  //Set data for object
  float half_size = 1.0f;
  vertexArray_.positions = {
    glm::vec3(-half_size,-half_size,half_size),
    glm::vec3(half_size,-half_size,half_size),
    glm::vec3(half_size,half_size,half_size),
    glm::vec3(-half_size,half_size,half_size),

    glm::vec3(-half_size,-half_size,-half_size),
    glm::vec3(half_size,-half_size,-half_size),
    glm::vec3(half_size,half_size,-half_size),
    glm::vec3(-half_size,half_size,-half_size)
  };

  vertexArray_.normals = {
    glm::vec3(0.0f,0.0f,1.0f),
    glm::vec3(0.0f,0.0f,1.0f),
    glm::vec3(0.0f,0.0f,1.0f),
    glm::vec3(0.0f,0.0f,1.0f),
    glm::vec3(0.0f,0.0f,-1.0f),
    glm::vec3(0.0f,0.0f,-1.0f),
    glm::vec3(0.0f,0.0f,-1.0f),
    glm::vec3(0.0f,0.0f,-1.0f)
  };

  vertexArray_.texCoords = {
    glm::vec2(1.0f,1.0f),
    glm::vec2(0.0f,1.0f),
    glm::vec2(0.0f,0.0f),
    glm::vec2(1.0f,0.0f),

    glm::vec2(0.0f,1.0f),
    glm::vec2(1.0f,1.0f),
    glm::vec2(1.0f,0.0f),
    glm::vec2(0.0f,0.0f)
  };

  indices_ = {
    0,1,2,3,
    1,5,6,2,
    3,2,6,7,

    4,7,6,5,
    0,3,7,4,
    4,5,1,0
  };
  //add indices to vertexArray_
  for(unsigned int i = 0; i < indices_.size(); i++)
    vertexArray_.indices.push_back(indices_[i]);


  //Initialize Shader
  cubeShader_.initShaders("D:/slimk/Documents/VR/SpaceShooters/shaders/Skybox");


   //Initialize Texture
  texture_.initCubeTextures("D:/slimk/Documents/VR/SpaceShooters/resources/right.png",
                            "D:/slimk/Documents/VR/SpaceShooters/resources/left.png",
                            "D:/slimk/Documents/VR/SpaceShooters/resources/top.png",
                            "D:/slimk/Documents/VR/SpaceShooters/resources/bot.png",
                            "D:/slimk/Documents/VR/SpaceShooters/resources/front.png",
                            "D:/slimk/Documents/VR/SpaceShooters/resources/back.png",
                            SKYBOX);


  //Initialize Mesh
  mesh_.createModel(&vertexArray_);


  //Edit Shader Uniforms
  cubeShader_.enable();
  glUniform1i(glGetUniformLocation(cubeShader_.getProg(), "u_SkyBox"), SKYBOX);
  cubeShader_.disable();
}

void SkyBox::privateRender()
{
  //Use Shaders
  cubeShader_.enable();

  //Use Textures
  //  glDisable(GL_LIGHTING);
  glDisable(GL_DEPTH_TEST);
  texture_.bindCube(SKYBOX);

  //Draw SkyBox
  mesh_.draw(GL_QUADS);

  //Disable Texture  
//  glEnable(GL_LIGHTING);
  glEnable(GL_DEPTH_TEST);
  texture_.unBindCube(SKYBOX);

  //Disable Shader
  cubeShader_.disable();
}


void SkyBox::privateUpdate()
{

}
