#include "../include/Water.hpp"
#include <glm/gtc/matrix_transform.hpp>

Water::Water()
{
  matrix_ = glm::mat4(1.0);
}

Water::~Water()
{

}

void Water::privateInit()
{
  //Start timer
  timer.start();

  //Set initial position of object
  matrix_ = glm::translate(matrix_, glm::vec3(nCols/-2.0f, 0.0f, offset_*nRows*-1.0f));
  int scaleX = 1;
  size_ = nCols*scaleX;
  for(unsigned int z = 0; z < nRows; ++z){
    for(unsigned int x = 0; x < nCols; ++x){
      vertexArray_.positions.push_back(glm::vec3(x,0,z));
      vertexArray_.texCoords.push_back(glm::vec2(float(x)/float(nCols),float(z)/float(nRows)));
    }
  }

  for(unsigned int z = 0; z < (nRows-1); ++z){
    for(unsigned int x = 0; x < (nCols-1); ++x){
      vertexArray_.indices.push_back(z*nCols + x);
      vertexArray_.indices.push_back(z*nCols + x+1);
      vertexArray_.indices.push_back((z+1)*(nCols)+x+1);
      vertexArray_.indices.push_back((z+1)*(nCols)+x);
    }
  }


  //Initialize Shader
  waterShader_.initShaders("D:/slimk/Documents/VR/SpaceShooters/shaders/waterReflection");


  //Initialize Texture
  texture_.initCubeTextures("D:/slimk/Documents/VR/SpaceShooters/resources/skybox_right.bmp",
                            "D:/slimk/Documents/VR/SpaceShooters/resources/skybox_left.bmp",
                            "D:/slimk/Documents/VR/SpaceShooters/resources/skybox_top.bmp",
                            "D:/slimk/Documents/VR/SpaceShooters/resources/skybox_bot.bmp",
                            "D:/slimk/Documents/VR/SpaceShooters/resources/skybox_front.bmp",
                            "D:/slimk/Documents/VR/SpaceShooters/resources/skybox_back.bmp",
                            WATERSKYBOX);

  //Initialize Mesh
  mesh_.createModel(&vertexArray_);

  //Edit Shader Uniforms
  waterShader_.enable();
  glUniform1i(glGetUniformLocation(waterShader_.getProg(), "skybox"), WATERSKYBOX);
  waterShader_.disable();

}

void Water::privateRender()
{
  double lap = timer.elapsed();
  if (timer.elapsed() >= 15.0){
    while (lap >= 0){
      lap--;
    }
    timer.reset();
      timer.start();
      lap = timer.elapsed();
      std::cout << "here we go again" << std::endl;
    }



  //Use Shaders
  waterShader_.enable();
  glUniform1f(glGetUniformLocation(waterShader_.getProg(), "u_time"), lap);


  //Use Textures
  texture_.bindCube(WATERSKYBOX);

  //Draw
  mesh_.draw(GL_QUADS);

  //Disable Texture
  texture_.unBind2D(WATERSKYBOX);

  //Disable Shader
  waterShader_.disable();
}

void Water::privateUpdate()
{

}
