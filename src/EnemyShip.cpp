#include "../include/EnemyShip.hpp"
#include <iostream>
#include <ctime>

EnemyShip::EnemyShip()
{
  //Set initial position of object
  matrix_ = glm::mat4(1.0);

  //Set some properties
  life_ = 100;
  speed_ = 0.05f;
  size_ = 5.0f;
  impact = 80;
}

EnemyShip::~EnemyShip()
{
}

void EnemyShip::privateInit()
{
  //Start timer
  timer.start();


  //Set data for object
  float half_size = size_/2.0f;
  vertexArray_.positions = {
    glm::vec3(-half_size,-half_size,half_size),
    glm::vec3(half_size,-half_size,half_size),
    glm::vec3(half_size,half_size,half_size),
    glm::vec3(-half_size,half_size,half_size),
    glm::vec3(-half_size,-half_size,-half_size),
    glm::vec3(half_size,-half_size,-half_size),
    glm::vec3(half_size,half_size,-half_size),
    glm::vec3(-half_size,half_size,-half_size)
  };

  vertexArray_.colors = {
    glm::vec3(0.0f,0.0f,0.0f),
    glm::vec3(0.0f,0.0f,1.0f),
    glm::vec3(0.0f,1.0f,0.0f),
    glm::vec3(1.0f,1.0f,1.0f),
    glm::vec3(1.0f,0.0f,0.0f),
    glm::vec3(1.0f,0.0f,1.0f),
    glm::vec3(1.0f,1.0f,0.0f),
    glm::vec3(1.0f,1.0f,1.0f)
  };

  vertexArray_.normals = {
    glm::vec3(0.0f,0.0f,1.0f),
    glm::vec3(0.0f,0.0f,1.0f),
    glm::vec3(0.0f,0.0f,1.0f),
    glm::vec3(0.0f,0.0f,1.0f),
    glm::vec3(0.0f,0.0f,-1.0f),
    glm::vec3(0.0f,0.0f,-1.0f),
    glm::vec3(0.0f,0.0f,-1.0f),
    glm::vec3(0.0f,0.0f,-1.0f)
  };

  vertexArray_.texCoords = {
    glm::vec2(0.0f,0.0f),
    glm::vec2(1.0f,0.0f),
    glm::vec2(1.0f,1.0f),
    glm::vec2(0.0f,1.0f),
    glm::vec2(0.0f,0.0f),
    glm::vec2(1.0f,0.0f),
    glm::vec2(1.0f,1.0f),
    glm::vec2(0.0f,1.0f)
  };

  float offset = 0.5f;
  srand(std::time(0));
  for(unsigned int i = 0; i < amount; ++i){
    float displacement = (rand() % (int)(2 * offset * 100)) / 100.0f - offset;
    float x = 3 * i + displacement ;
    vertexArray_.tranformations.push_back(glm::vec3(x,0,0));
    disp_x += x;
  }
  disp_x /= amount;
  std::cout << disp_x << std::endl;

  //  vertexArray_.tranformations = {
  //    glm::vec3(0.0f,0.0f,1.0f),
  //    glm::vec3(3.5f,0.0f,1.0f),
  //    glm::vec3(7.0f,0.0f,1.0f),
  //    glm::vec3(10.0f,0.0f,1.0f)
  //  };

  indices_ = {
    0,1,2,3,
    1,5,6,2,
    3,2,6,7,

    4,7,6,5,
    0,3,7,4,
    4,5,1,0
  };
  for(unsigned int i = 0; i < indices_.size(); i++)
    vertexArray_.indices.push_back(indices_[i]);


  //Initialize Shader
  shipShader_.initShaders("D:/slimk/Documents/VR/SpaceShooters/shaders/phong");


  //Initialize Texture
  texture_.init2DTextures("D:/slimk/Documents/VR/SpaceShooters/resources/test_rect.png", PUZZLE);
  texture_.init2DTextures("D:/slimk/Documents/VR/SpaceShooters/resources/img_test.png", COLORFUL);


  //Initialize Mesh
  mesh_.createModel(&vertexArray_);


  //Edit Shader Uniforms
  shipShader_.enable();
  glUniform1i(glGetUniformLocation(shipShader_.getProg(), "u_Tex0"), PUZZLE);
  glUniform1i(glGetUniformLocation(shipShader_.getProg(), "u_Texture"), COLORFUL);
  shipShader_.disable();

  //  matrix_ = glm::translate(glm::mat4(1.0), glm::vec3((-1*float(disp_x)), 0.0f, -500.0f));
  matrix_ = glm::translate(glm::mat4(1.0), glm::vec3(xPlace_ + (-1*float(disp_x)), 0.0f, zPlace_));
  directionTimer.start();

  rightBoundary_ = true;
  leftBoundary_ = true;

}

void EnemyShip::privateRender()
{
  //Transform Object
  matrix_ = glm::translate(matrix_, glm::vec3(0.0f, 0.0f, 0.0f));

  //Use Shaders
  shipShader_.enable();
  glUniform1f(glGetUniformLocation(shipShader_.getProg(), "u_Time"), timer.elapsed());

  //Use Textures
  texture_.bind2D(PUZZLE);
  texture_.bind2D(COLORFUL);

  //Draw
  //if (getType is 1)
  mesh_.draw(GL_QUADS);
  //else if (getType is 2)
  //mesh2.draw..

  //Disable Texture
  texture_.unBind2D(COLORFUL);
  texture_.unBind2D(PUZZLE);

  //Disable Shader
  shipShader_.disable();
}

void EnemyShip::privateUpdate()
{
  matrix_ = glm::translate(matrix_, glm::vec3(0.0f, 0.0f, -speed_/10));

  srand(std::time(0));
  if (rand() % (int)(2) == 1)
    moveLeft();
  else
    moveRight();

  if (timer.elapsed() >= 1.0){
    reload_ = true;
    timer.reset();
    timer.start();
  }
  else{
    reload_ = false;
  }

}

void EnemyShip::setZPlace(float place)
{
  zPlace_ = place;
}

void EnemyShip::setXPlace(float place)
{
  xPlace_ = place;
}

void EnemyShip::setSpeed(float speed)
{
  speed_ = speed;
}

void EnemyShip::damaged(int hit)
{
  while (hit > 0) {
      life_ -= 1;
      --hit;
    }
  }


unsigned int EnemyShip::getLife()
{
  return life_;
}

unsigned int EnemyShip::getArmor()
{
  return armor_;
}

unsigned int EnemyShip::getImpact()
{
  return impact;
}

float EnemyShip::getSpeed()
{
  return speed_;
}

float EnemyShip::getHalfSize()
{
  return size_/2;
}

bool EnemyShip::getReload()
{
  return reload_;
}

void EnemyShip::setFlag(int value)
{
  flag = value;
}

void EnemyShip::moveForward()
{
  matrix_ = glm::translate(matrix_, glm::vec3(0.0f, 0.0f, -speed_));
}

void EnemyShip::moveRight()
{
  if(moveRightFlag){
    if (rightBoundary_){
      moveLeftFlag = false;
      matrix_ = glm::translate(matrix_, glm::vec3(-speed_, 0.0f, 0.0f));
    }
  }
  moveLeftFlag = true;
}

void EnemyShip::moveLeft()
{
  if(moveLeftFlag){
    if (leftBoundary_){
      moveRightFlag = false;
      matrix_ = glm::translate(matrix_, glm::vec3(speed_, 0.0f, 0.0f));
    }
  }
  moveRightFlag = true;
}

void EnemyShip::setLeftBoundary(bool value)
{
  leftBoundary_ = value;
}

void EnemyShip::setRightBoundary(bool value)
{
  rightBoundary_ = value;
}
