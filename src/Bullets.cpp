#include "../include/Bullets.hpp"

#include "../include/EnemyShip.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <GL/freeglut.h>

#include <iostream>


Bullets::Bullets()
{
  //Set initial position of object
  matrix_ = glm::mat4(1.0);

  //Set some properties
  size_ = 0.5f;
  spin_ = 0.0f;
  speed_ = 0.3f;
  if(type_ == 1)
    impact = 50;
  else impact = 10;
}

Bullets::~Bullets()
{
}

void Bullets::privateInit()
{
//  matrix_ = glm::translate(matrix_, startPos_);
  matrix_ = startPos_;

}

void Bullets::privateRender()
{
  if(type_ == 0){
    impact = 10;
    glColor3f(1.0, 0.5, 0.8);
    glutSolidSphere(size_, 16, 16);
    glEnd();

  }
  else if(type_ == 1){
    impact = 50;
    glColor3f(1.0, 0.5, 0.2);
    glutSolidSphere(size_, 16, 16);
    glEnd();
  }
}

void Bullets::privateUpdate()
{

  glm::mat4 trans = glm::translate(glm::mat4(1), direction_*speed_);
  matrix_ = matrix_ * trans ;
//  glTranslatef(direction_.x*speed_, direction_.y*speed_,direction_.z*speed_);
  //Update matrix based on speed??
}


void Bullets::setStartPos(const glm::mat4 v)
{
  startPos_ = v;
}

void Bullets::setType(int type)
{
  type_ = type;
}

void Bullets::setDir(glm::vec3 dir)
{
//  auto mult = 300;
//  direction_.x = dir;
  direction_ = dir;
//  setDirection(glm::vec3{direction[0] * mult, direction[1] * mult, direction[2] * mult});
}


void Bullets::spin()
{
  spin_++;
  if (spin_ > 360.0f)
    spin_ = spin_ - 360.0f;
}


float Bullets::getSpeed()
{
  return speed_;
}

unsigned int Bullets::getImpact()
{
  return impact;
}

float Bullets::getHalfSize()
{
  return size_/2;
}
