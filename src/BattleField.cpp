#include "../include/BattleField.hpp"
#include <glm/gtc/matrix_transform.hpp>

BattleField::BattleField()
{
  matrix_ = glm::mat4(1.0);
}

BattleField::~BattleField()
{
}

void BattleField::privateInit()
{
  //Set initial position of object
  int scaleX = 1, scaleZ = 1;
  matrix_ = glm::translate(matrix_, glm::vec3(nCols*scaleX/-2.0f, 0.0f, offset_*nRows*scaleZ*-1.0f));
  size_ = nCols*scaleX;
  for(unsigned int z = 0; z < nRows; ++z){
    for(unsigned int x = 0; x < nCols; ++x){
      vertexArray_.positions.push_back(glm::vec3(scaleX*x,0,scaleZ*z));
      vertexArray_.texCoords.push_back(glm::vec2(float(x)/float(nCols),float(z)/float(nRows)));
    }
  }

  for(unsigned int z = 0; z < (nRows-1); ++z){
    for(unsigned int x = 0; x < (nCols-1); ++x){
      vertexArray_.indices.push_back(z*nCols + x);
      vertexArray_.indices.push_back(z*nCols + x+1);
      vertexArray_.indices.push_back((z+1)*(nCols)+x+1);
      vertexArray_.indices.push_back((z+1)*(nCols)+x);
    }
  }


  //Initialize Shader
  floorShader_.initShaders("D:/slimk/Documents/VR/SpaceShooters/shaders/terrain");


  //Initialize Texture
  texture_.init2DTextures("D:/slimk/Documents/VR/SpaceShooters/resources/height_map.bmp", HEIGHTMAP);
  texture_.init2DTextures("D:/slimk/Documents/VR/SpaceShooters/resources/color_map.bmp", COLORMAP);
  texture_.init2DTextures("D:/slimk/Documents/VR/SpaceShooters/resources/light_map.bmp", LIGHTMAP);
  texture_.init2DTextures("D:/slimk/Documents/VR/SpaceShooters/resources/normal_map.bmp", NORMALMAP);


  //Initialize Mesh
  mesh_.createModel(&vertexArray_);


  //Edit Shader Uniforms
  floorShader_.enable();
  glUniform1i(glGetUniformLocation(floorShader_.getProg(), "u_heightMap"), HEIGHTMAP);
  glUniform1i(glGetUniformLocation(floorShader_.getProg(), "u_colorMap"), COLORMAP);
  glUniform1i(glGetUniformLocation(floorShader_.getProg(), "u_lightMap"), LIGHTMAP);
  glUniform1i(glGetUniformLocation(floorShader_.getProg(), "u_normalMap"), NORMALMAP);
  floorShader_.disable();
}

void BattleField::privateRender()
{
  //Use Shaders
  floorShader_.enable();

  //Use Textures
  texture_.bind2D(HEIGHTMAP);
  texture_.bind2D(COLORMAP);

  //Draw
  mesh_.draw(GL_QUADS);

  //Disable Texture
  texture_.unBind2D(COLORMAP);
  texture_.unBind2D(HEIGHTMAP);

  //Disable Shader
  floorShader_.disable();
}

void BattleField::privateUpdate()
{
}

float BattleField::getHalfSize()
{
  return size_/2;
}

void BattleField::setOffset(float zOff)
{
  offset_ = zOff;
}

