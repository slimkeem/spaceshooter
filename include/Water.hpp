#ifndef WATER_HPP
#define WATER_HPP

#pragma once
#include <windows.h>
#include <glm/glm.hpp>

#include "../utils/Mesh.hpp"
#include "../utils/Shader.hpp"
#include "../utils/Texture.hpp"
#include "../utils/Structures.hpp"
#include "../utils/ObjectLoader.hpp"
#include "../include/Clock.hpp"
#include "../include/Terrain.hpp"
#include "../utils/RenderText.hpp"


enum WaterTextures
{
  WATERSKYBOX
};
class Water : public SceneObject
{
  public:
    Water();
    ~Water();

  protected:
    virtual void privateInit();
    virtual void privateRender();
    virtual void privateUpdate();

  private:
    VertexArray vertexArray_;
    Texture texture_;
    Shader waterShader_;
    Mesh mesh_;
    siut::Clock timer;

    unsigned int nCols = 64;
    unsigned int nRows = 128;
    unsigned int size_;
    float offset_ = 1.0f, wavy_ = 0.0f;
    bool reverse = false;
};

#endif // WATER_HPP
