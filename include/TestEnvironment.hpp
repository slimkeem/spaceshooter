#pragma once

#include <windows.h>
#include <iostream>
#include <map>
#include <string>

#include "../utils/Mesh.hpp"
#include "../utils/Shader.hpp"
#include "../utils/Texture.hpp"
#include "../utils/Structures.hpp"
#include "../utils/ObjectLoader.hpp"
#include "../utils/RenderText.hpp"

#include "../include/Clock.hpp"

#include "SceneObject.hpp"


class TestEnvironment : public SceneObject
{
public:
  TestEnvironment();
  ~TestEnvironment();

  bool faded{ false };

  unsigned int FirstUnusedParticle();
protected:
  void privateInit();
  void privateRender();
  void privateUpdate();


private:
  static constexpr int MAX_PARTICLES{ 500 };
  Particles particles_[MAX_PARTICLES];
  Shader particleShader;
  Texture particleTexture;

  float _slowdown = 2000.0f;

  void prepareRender();
  void disableRender();

  RenderText textCharacter;
};

