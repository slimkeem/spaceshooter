#pragma once

#include <windows.h>

#include "SpaceShip.hpp"
#include "Bullets.hpp"
#include "TestEnvironment.hpp"
#include "SceneObject.hpp"
#include "BattleField.hpp"
#include "EnemyShip.hpp"
#include "Camera.hpp"
#include "SkyBox.hpp"
#include "Clock.hpp"
#include "Water.hpp"
#include "../utils/RenderText.hpp"

#include <GL/gl.h>
#include <GL/glu.h>

//#include <random>

class GameManager : public SceneObject
{
	public:
		GameManager();
		~GameManager();
    
    std::shared_ptr<Camera> getCam();
    std::shared_ptr<SpaceShip> getSpaceShip();
    std::shared_ptr<Bullets> getBullet();

    void playerShoot();
    void toggleWeapon();
  protected:
    virtual void privateInit();
		virtual void privateRender();
		virtual void privateUpdate();


  private:
    std::shared_ptr<BattleField> bf_;
    std::shared_ptr<BattleField> bf2_;
    std::shared_ptr<SpaceShip> spaceship_;
    std::shared_ptr<EnemyShip> enemyship_;
    std::shared_ptr<SkyBox> skybox_;
    std::shared_ptr<Camera> cam_;
    std::shared_ptr<TestEnvironment> particle_;
    std::shared_ptr<Bullets> bullet_;
    std::shared_ptr<Water> water_;


//    std::vector<std::shared_ptr<BattleField> > terrains_;
    std::vector<std::shared_ptr<EnemyShip> > enemies_;
    std::vector<std::shared_ptr<Bullets> > enemyBullets_;
    std::vector<std::shared_ptr<Bullets> > playerBullets_;
    std::vector<std::shared_ptr<TestEnvironment> > particles_;

    siut::Clock timer_, shootTimer_;
    RenderText textCharacter;

//    std::random_device dev;
//    std::mt19937 gen;

    bool terrainFlag, terrain2Flag;
    bool start = true;
    bool isGameActive = true;
    unsigned int wave, flag =1, level = 1;
    float offset1=1.0f;
    float offset2=0.0f;
    float zPlace, xPlace;
    unsigned int highScore = 0;


    void spawnEnemyShips();
    void collisionDetections();
    void checkOutOfBounds();
    void enemyShoot();
    void setStartPos();
    bool checkEnemyPlayerCollision(std::shared_ptr<EnemyShip> enemy);
    bool checkBulletPlayerCollision(std::shared_ptr<Bullets> bullet);
    void checkBulletPlayerHit();
    void checkEnemyPlayerHit();
    void setBoundaryToTerrain();
    void spawnTerrains();
    void checkBulletEnemyHit();
    void checkBulletBulletHit();
    bool checkBulletBulletCollision(std::shared_ptr<Bullets> enemyBullet, std::shared_ptr<Bullets> playerBullet);
    bool checkBulletEnemyCollision(std::shared_ptr<EnemyShip> enemy, std::shared_ptr<Bullets> bullet);
    void removeParticles();
};

