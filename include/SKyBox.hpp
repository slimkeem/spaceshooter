#pragma once

#include <windows.h>

#include "../utils/Mesh.hpp"
#include "../utils/Shader.hpp"
#include "../utils/Texture.hpp"
#include "../utils/Structures.hpp"
#include "../utils/ObjectLoader.hpp"

enum SkyTextures
{
  SKYBOX
};

class SkyBox : public SceneObject
{
  public:
    SkyBox();
    ~SkyBox();

    void setPos(const glm::vec3 &vec);


  protected:
    void privateInit();
    void privateRender();
    void privateUpdate();


  private:
    VertexArray vertexArray_;
    Texture texture_;
    Shader cubeShader_;
    Mesh mesh_;

    std::vector<int> indices_;

};

//#endif
