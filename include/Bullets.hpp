#pragma once

#include <windows.h>

#include "../utils/Mesh.hpp"
#include "../utils/Shader.hpp"
#include "../utils/Texture.hpp"
#include "../utils/Structures.hpp"
#include "../utils/ObjectLoader.hpp"
#include "../include/Clock.hpp"

#include "SceneObject.hpp"

enum BulletTextures
{
  ROCKET
};

class Bullets : public SceneObject
{
	public:
    Bullets();
    ~Bullets();

    void spin();
    void moveRight();
    void moveLeft();
    void damaged(int hit);

    float getHalfSize();
    float getSpeed();
    unsigned int getImpact();

    void shootWeapon(int geom_);
    void setStartPos(const glm::mat4 v);
    void setType(int type);
    void setDir(glm::vec3 dir);
protected:
    void privateInit();
		void privateRender();
		void privateUpdate();


	private:
    float speed_;
    unsigned int type_ = 0;
    unsigned int impact;
    float size_;
    glm::mat4 startPos_ = glm::mat4(1.0);
    glm::vec3 direction_ = glm::vec3(0.0f, 0.0f, 0.1f);

    VertexArray vertexArray_;
    std::vector<int> indices_;

    Mesh mesh_;
    Mesh planet_;
    Shader shipShader_;
    Texture texture_;
    siut::Clock timer;

    float spin_;
};

