#pragma once

#include <windows.h>

#include "../utils/Mesh.hpp"
#include "../utils/Shader.hpp"
#include "../utils/Texture.hpp"
#include "../utils/Structures.hpp"
#include "../utils/ObjectLoader.hpp"
#include "../include/Clock.hpp"

#include "SceneObject.hpp"

enum ShipTextures
{
  PLANET
};

class SpaceShip : public SceneObject
{
	public:
		SpaceShip();
		~SpaceShip();

    void spin(float aimX);
    void moveRight();
    void moveLeft();
    void damaged(int hit);

    unsigned int getLife();
    float getHalfSize();
    float getSpeed();
    unsigned int getArmor();

    void setLeftBoundary(bool value);
    void setRightBoundary(bool value);

    void setPoints(unsigned int point);

    void moveForward();

    void losePoints(unsigned int hit);

    bool getReload();

    float getSpin();

    unsigned int getPoint();

    void gainHealth(unsigned int point);
protected:
    void privateInit();
		void privateRender();
		void privateUpdate();


	private:
    float speed_;
    unsigned int life_;
    unsigned int armor_;
    unsigned int point_;
    float size_;
    bool reload_ = false;
    bool rightBoundary_ = true, leftBoundary_ = true;

    VertexArray vertexArray_;
    std::vector<int> indices_;

    Mesh mesh_;
    Mesh rocket;
    Mesh planet_;
    Shader shipShader_;
    Texture texture_;
    siut::Clock timer;

    float spin_;
};

