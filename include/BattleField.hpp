#pragma once
#include <windows.h>
#include <glm/glm.hpp>

#include "../utils/Mesh.hpp"
#include "../utils/Shader.hpp"
#include "../utils/Texture.hpp"
#include "../utils/Structures.hpp"
#include "../utils/ObjectLoader.hpp"
#include "../include/Clock.hpp"
#include "../include/Terrain.hpp"
#include "../utils/RenderText.hpp"

enum TerrainTextures
{
  HEIGHTMAP,
  COLORMAP,
  LIGHTMAP,
  NORMALMAP
};

class BattleField : public SceneObject
{
  public:
    BattleField();
    ~BattleField();

    float getHalfSize();

    void setOffset(float zOff);

  protected:
    virtual void privateInit();
    virtual void privateRender();
    virtual void privateUpdate();

  private:
    VertexArray vertexArray_;
    Texture texture_;
    Shader floorShader_;
    Mesh mesh_;


    unsigned int nCols = 64;
    unsigned int nRows = 512;
    unsigned int size_;
    float offset_ = 1.0f;
};


