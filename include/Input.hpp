#pragma once

const int KEY_ID_W        = 0;
const int KEY_ID_S        = 1;
const int KEY_ID_A        = 2;
const int KEY_ID_D        = 3;
const int KEY_ID_SPACE    = 4;
const int KEY_ID_C        = 5;

const int MOUSE_MIDDLE_BUTTON_DOWN = 10;
const int MOUSE_RIGHT_BUTTON_DOWN = 11;
const int MOUSE_LEFT_BUTTON_DOWN = 20;
