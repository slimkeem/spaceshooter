#pragma once

#include <windows.h>

#include "../utils/Mesh.hpp"
#include "../utils/Shader.hpp"
#include "../utils/Texture.hpp"
#include "../utils/Structures.hpp"
#include "../utils/ObjectLoader.hpp"
#include "../include/Clock.hpp"

#include "SceneObject.hpp"


#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

enum EnemyTextures
{
  PUZZLE,
  COLORFUL
};

class EnemyShip : public SceneObject
{
	public:
    EnemyShip();
    ~EnemyShip();

    void damaged(int hit);
    unsigned int getLife();
    float getHalfSize();
    float getSpeed();
    unsigned int getArmor();
    unsigned int getImpact();


    bool getReload();

    void setFlag(int value);

    void setLeftBoundary(bool value);
    void setRightBoundary(bool value);

    void setZPlace(float place);
    void setXPlace(float place);
    void setSpeed(float speed);

    void moveRight();
    void moveLeft();

protected:
    void privateInit();
    void privateRender();
    void privateUpdate();


  private:
    void moveForward();

	  float speed_;
    unsigned int life_;
    unsigned int armor_;
    float size_;
    float zPlace_;
    float xPlace_;
    unsigned int impact;
    bool reload_ = false;
    bool rightBoundary_ = true, leftBoundary_ = true;
    bool moveRightFlag = false, moveLeftFlag = false;

    int flag = 0;
    unsigned int amount = 3;
    unsigned int disp_x = 0;

    VertexArray vertexArray_;
    std::vector<int> indices_;

    Mesh mesh_;
    Shader shipShader_;
    Texture texture_;
    siut::Clock timer, directionTimer;
};


