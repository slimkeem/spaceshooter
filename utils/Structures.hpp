#ifndef VERTEXARRAY_HPP
#define VERTEXARRAY_HPP

#include "../include/SceneObject.hpp"

struct VertexArray
{
  std::vector< glm::vec3 >          positions;
  std::vector< glm::vec2 >          texCoords;
  std::vector< glm::vec3 >          normals;
  std::vector< glm::vec3 >          colors;
  std::vector< glm::vec3 >          tranformations;
  std::vector< unsigned int >       indices;
};

struct Character {
    unsigned int                    TextureID;  // ID handle of the glyph texture
    glm::ivec2                      Size;       // Size of glyph
    glm::ivec2                      Bearing;    // Offset from baseline to left/top of glyph
    unsigned int                    Advance;    // Horizontal offset to advance to next glyph
};

struct Particles {
  bool active;
      float life;
      float fade;
      // Position values
      float x, y, z;
      // Direction values
      float xi, yi, zi;
      // Gravity values
      float xg, yg, zg;
};

enum BufferPositions
{
  POSITION_VB,
  TEXCOORD_VB,
  NORMAL_VB,
  COLOR_VB,
  TRANS_VB,
  INDEX_VB
};

////move away from here
//enum Textures
//{
//  PUZZLE,
//  COLORFUL,
//  SKYBOX,
//  LEFT,
//  HEIGHTMAP,
//  COLORMAP
//};

#endif // VERTEXARRAY_HPP
