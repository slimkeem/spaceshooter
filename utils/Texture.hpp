#ifndef TEXTURE_H
#define TEXTURE_H

#include <string>
#include <GL/glew.h>

class Texture
{
public:
  Texture();
  virtual ~Texture();

  void init2DTextures(char *fileName, int slot);
  void initCubeTextures(char *fileName1,
                        char *fileName2,
                        char *fileName3,
                        char *fileName4,
                        char *fileName5,
                        char *fileName6,
                        int slot);
//  void initHeightMap(char *fileName, int slot);

  void bind2D(unsigned int slot) const;
  void bindCube(unsigned int slot) const;
//  void bindHeightMap(unsigned int slot) const;

  void unBind2D(unsigned int slot) const;
  void unBindCube(unsigned int slot) const;
//  void unBindHeightMap() const;

protected:
private:
  Texture(const Texture& texture) {}
  void operator=(const Texture& texture) {}

  static const unsigned int NUM_TEXTURES = 32;

  unsigned int texture_[NUM_TEXTURES];
  unsigned char *height_[NUM_TEXTURES];

};

#endif
