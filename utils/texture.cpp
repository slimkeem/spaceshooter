#include "Texture.hpp"
#include <SOIL/SOIL.h>
#include <iostream>

Texture::Texture()
{
}

Texture::~Texture()
{
  glDeleteTextures(NUM_TEXTURES, texture_);
}

void Texture::init2DTextures(char *fileName, int slot)
{
  glActiveTexture(GL_TEXTURE0 + slot);
  glBindTexture(GL_TEXTURE_2D, texture_[slot]);
  texture_[slot] = SOIL_load_OGL_texture(
        fileName,
        SOIL_LOAD_AUTO,
        SOIL_CREATE_NEW_ID,
        SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT
        );

  if( 0 == texture_[slot] )
    std::cout<<( "Texture cpp SOIL loading error: \n") << (SOIL_last_result()) <<std::endl;

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_NEAREST);
}

void Texture::initCubeTextures(char *fileName1,
                               char *fileName2,
                               char *fileName3,
                               char *fileName4,
                               char *fileName5,
                               char *fileName6, int slot)
{
  glActiveTexture(GL_TEXTURE0 + slot);
  glBindTexture(GL_TEXTURE_CUBE_MAP, texture_[slot]);
  texture_[slot] = SOIL_load_OGL_cubemap(
        fileName1,
        fileName2,
        fileName3,
        fileName4,
        fileName5,
        fileName6,
        SOIL_LOAD_RGB,
        SOIL_CREATE_NEW_ID,
        SOIL_FLAG_MIPMAPS
        );
  if( 0 == texture_[slot] )
    std::cout<<( "Texture cpp SOIL loading error: \n") << (SOIL_last_result()) <<std::endl;

  glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_NORMAL_MAP);
  glTexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_NORMAL_MAP);
  glTexGeni(GL_R, GL_TEXTURE_GEN_MODE, GL_NORMAL_MAP);

  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
}

void Texture::bind2D(unsigned int slot) const
{
  glEnable(GL_TEXTURE_2D);
  glActiveTexture(GL_TEXTURE0 + slot);
  glBindTexture(GL_TEXTURE_2D, texture_[slot]);
}

void Texture::bindCube(unsigned int slot) const
{
  //  glFrontFace(GL_CW);
//  glEnable(GL_TEXTURE_GEN_S);
//  glEnable(GL_TEXTURE_GEN_T);
//  glEnable(GL_TEXTURE_GEN_R);
  glEnable(GL_TEXTURE_CUBE_MAP);
  glActiveTexture(GL_TEXTURE0 + slot);
  glBindTexture(GL_TEXTURE_CUBE_MAP, texture_[slot]);
}

void Texture::unBind2D(unsigned int slot) const
{
  glActiveTexture(GL_TEXTURE0 + slot);
  glDisable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, 0);
}

void Texture::unBindCube(unsigned int slot) const
{
  glActiveTexture(GL_TEXTURE0 + slot);
//  glDisable(GL_TEXTURE_GEN_S);
//  glDisable(GL_TEXTURE_GEN_T);
//  glDisable(GL_TEXTURE_GEN_R);
  glDisable(GL_TEXTURE_CUBE_MAP);
  glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
}
