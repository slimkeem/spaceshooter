#include "Mesh.hpp"
#include <iostream>

Mesh::Mesh()
{
}

Mesh::~Mesh()
{
  glDeleteBuffers(NUM_BUFFERS, vbo_);
  glDeleteVertexArrays(1, &vao_);
}

void Mesh::loadModel(const std::string &fileName)
{
  setupVA(OBJModel(fileName).ToIndexedModel());
}

void Mesh::createModel(VertexArray *vertexArray_)
{
  IndexedModel model;

  model.positions = vertexArray_->positions;
  model.texCoords = vertexArray_->texCoords;
  model.normals = vertexArray_->normals;
  model.indices = vertexArray_->indices;

  colorVec = vertexArray_->colors;
  transVec = vertexArray_->tranformations;

  setupVA(model);
}

void Mesh::draw(GLenum mode/*, unsigned int amount*/)
{
  glBindVertexArray(vao_);
  glDrawElementsBaseVertex(mode, m_numIndices, GL_UNSIGNED_INT, 0, 0);
//  glDrawElementsInstancedBaseVertex(mode, m_numIndices, GL_UNSIGNED_INT, 0, amount, 0);
  glBindVertexArray(0);
}

void Mesh::setupVA(const IndexedModel &model)
{
  m_numIndices = model.indices.size();

  //Generate and bind vertex array object
  glGenVertexArrays(1, &vao_);
  glBindVertexArray(vao_);

  //Generate vertex buffer object
  glGenBuffers(NUM_BUFFERS, vbo_);

  //Copy Position data to vbo
  glBindBuffer(GL_ARRAY_BUFFER, vbo_[POSITION_VB]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(model.positions[0]) * model.positions.size(), &model.positions[0], GL_STATIC_DRAW);
  //Link Position to slot: posAttrib
  glEnableVertexAttribArray(POSITION_VB);
  glVertexAttribPointer(POSITION_VB, 3, GL_FLOAT, GL_FALSE, sizeof(model.positions[0]), 0);

  //Copy Texture data to vbo
  glBindBuffer(GL_ARRAY_BUFFER, vbo_[TEXCOORD_VB]);
  if (!model.texCoords.empty())
    glBufferData(GL_ARRAY_BUFFER, sizeof(model.texCoords[0]) * model.texCoords.size(), &model.texCoords[0], GL_STATIC_DRAW);
  //Link TexCoords to slot: texAttrib
  glEnableVertexAttribArray(TEXCOORD_VB);
  glVertexAttribPointer(TEXCOORD_VB, 2, GL_FLOAT, GL_FALSE, 0, 0);

  //Copy Normal to vbo
  glBindBuffer(GL_ARRAY_BUFFER, vbo_[NORMAL_VB]);
  if (!model.normals.empty())
    glBufferData(GL_ARRAY_BUFFER, sizeof(model.normals[0]) * model.normals.size(), &model.normals[0], GL_STATIC_DRAW);
  //Link Normal to slot: normAttrib
  glEnableVertexAttribArray(NORMAL_VB);
  glVertexAttribPointer(NORMAL_VB, 3, GL_FLOAT, GL_FALSE, 0, 0);

  //Copy Color to vbo
  glBindBuffer(GL_ARRAY_BUFFER, vbo_[COLOR_VB]);
  if (!colorVec.empty())
    glBufferData(GL_ARRAY_BUFFER, sizeof(colorVec[0]) * colorVec.size(), &colorVec[0], GL_STATIC_DRAW);
  //Link Color to slot: colorAttrib
  glEnableVertexAttribArray(COLOR_VB);
  glVertexAttribPointer(COLOR_VB, 3, GL_FLOAT, GL_FALSE, 0, 0);

  //Copy Transformations to vbo
  glBindBuffer(GL_ARRAY_BUFFER, vbo_[TRANS_VB]);
  if (!transVec.empty())
    glBufferData(GL_ARRAY_BUFFER, sizeof(transVec[0]) * transVec.size(), &transVec[0], GL_STATIC_DRAW);
  //Link Transformation to slot: transAttrib
  glEnableVertexAttribArray(TRANS_VB);
  glVertexAttribPointer(TRANS_VB, 3, GL_FLOAT, GL_FALSE, 0, 0);
  glVertexAttribDivisor(TRANS_VB,1);

  //Copy Index data to ibo
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_[INDEX_VB]);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(model.indices[0]) * model.indices.size(), &model.indices[0], GL_STATIC_DRAW);

  //Unbind vao
  glBindVertexArray(0);
}
