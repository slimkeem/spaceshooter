#ifndef RENDERTEXT_H
#define RENDERTEXT_H

#include <windows.h>
#include <iostream>
#include <map>
#include <string>
#include <GL/glew.h>

#include <ft2build.h>
#include FT_FREETYPE_H

#include "Shader.hpp"
#include "Structures.hpp"

class RenderText
{
  public:
    RenderText();
    virtual ~RenderText();

    void initText(glm::vec3 color);

    void drawText(std::string text,
                  GLfloat x, GLfloat y,
                  GLfloat scale);

  protected:

  private:
    void textTexture();
    void asciiLoop();

    std::map<GLchar, Character> Characters;
    unsigned int vao_, vbo_, texture_;
    Shader textShader_;

    FT_Library ft;
    FT_Face face;
};

#endif
