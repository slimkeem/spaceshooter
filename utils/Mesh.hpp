#ifndef MESH_INCLUDED_H
#define MESH_INCLUDED_H

#include <GL/glew.h>
//#include <glm/glm.hpp>
//#include <vector>

#include "Structures.hpp"
#include "ObjectLoader.hpp"

//#include <memory>

class Mesh
{
  public:
    Mesh();
    virtual ~Mesh();

    void loadModel(const std::string& fileName);
    void createModel(VertexArray *vertexArray_);
    void draw(GLenum mode);

  private:
    void setupVA(const IndexedModel& model);

    static const unsigned int NUM_BUFFERS = 6;

    unsigned int vao_;
    unsigned int vbo_[NUM_BUFFERS];
    unsigned int m_numIndices;
    std::vector<glm::vec3> colorVec;
    std::vector<glm::vec3> transVec;
};

#endif
