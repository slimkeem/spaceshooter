#extension GL_EXT_gpu_shader4 : enable

varying vec2 textCoord;
varying vec2 position;
uniform float fade;
uniform sampler2D texture;

void main(void)
{
    vec3 diffuse = texture2D(texture, textCoord.xy).rgb;
    gl_FragColor = vec4(diffuse, fade);

}
