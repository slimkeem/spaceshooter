
//  float timeFactor = sin(vert_texCoord.y*60.0 + u_time*2.0)/30.0;



#version 150
uniform samplerCube skybox;
in mat4 modelView;
in vec4 pos_eye;
in vec3 normal;
out vec4 diffuseColor;

void main(void)
{
//    gl_FragColor = vec4(0.0, 0.0, 0.8, 0.2);

    float ratio = 1.0/1.3333;
    vec3 eyeCoord = vec3(pos_eye);
    vec3 refracted = refract(normalize(eyeCoord), normalize(normal), ratio);
    vec4 refraction = vec4(inverse(modelView)*vec4(refracted,0.0));

    eyeCoord.x = -eyeCoord.x;

    vec3 reflection = reflect(-normalize(eyeCoord), normal);

    refraction.y = -refraction.y;
    vec4 refl = texture(skybox, vec3(refraction));
//    vec3 Fresnel = vec3( dot(eyeCoord, normal) );

    diffuseColor = refl;

//    diffuseColor.a = 0.4;
    diffuseColor.a =  0.8;
    diffuseColor.r -= 0.4;
    diffuseColor.g -= 0.5;
    diffuseColor.b -= 0.6;

}
