layout (location = 0) in vec3 position;

out vec3 vert_TexCoords;

//uniform mat4 u_Projection;
//uniform mat4 u_View;

void main(void)
{
//  vec4 pos = u_Projection * u_View * vec4(position, 1.0f);
  vec4 pos = gl_ModelViewProjectionMatrix * vec4(position,1.0f);
  gl_Position = pos.xyww;
  vert_TexCoords = position;


}
