layout(location=0) in vec3 position;
layout(location=1) in vec2 texCoord;

out vec2 var_TexCoord;
out vec3 var_Normal;

uniform sampler2D u_heightMap;

void main()
{
  vec4 in_Position = vec4(position, 1.0);
  var_TexCoord = texCoord;

  in_Position.y = texture2D(u_heightMap, var_TexCoord).x * 51.2 - 51.2;
  gl_Position = gl_ModelViewProjectionMatrix * in_Position;
}
