varying vec3 normalCoord;
uniform float u_time;
varying mat4 modelView;

varying vec4 pos_eye;
varying vec3 normal;
void main(void)
{
    vec4 pos = gl_Vertex;

    modelView = gl_ModelViewMatrix;

    pos_eye = gl_ModelViewMatrix * gl_Vertex;


//    pos.y = sin(( pos.z + randomBullshit)/(float(64))) + cos(pos.x/(float(64))) * float(5) - float(5);
    float factor = (sin(u_time * 3.0) + 1.0) / 2.0;
    pos.y = factor;
    pos.y = sin(( pos.z + u_time)/(float(200))) + cos(pos.x/(float(200))) - float(35);

    gl_Position = gl_ModelViewProjectionMatrix * pos;

    normalCoord = gl_NormalMatrix * gl_Normal;
    normal = normalCoord;
}
