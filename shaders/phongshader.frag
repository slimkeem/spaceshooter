in vec2 vert_TexCoord;
in  vec3 vert_Normal;

out vec4 outColor;

uniform sampler2D u_Texture;

void main () 
{

//    vec3 lightVec = vec3(gl_LightSource[0].position);
    vec3 lightVec = vec3(1.0f, 1.0f, 1.0f);
    lightVec = normalize(lightVec);
//    const vec3 diffuseMaterial = gl_FrontLightProduct[0].diffuse.xyz;
    const vec3 diffuseMaterial = vec3(1.0f, 0.6f, 0.0f);
    const vec3 specularMaterial = vec3(1.0f, 1.0f, 1.0f);
	
    vec3 NormalVec = normalize(vert_Normal);
    // calculate half angle vector
    vec3 eyeVec = vec3(0.0, 0.0, 1.0);
    vec3 halfVec = normalize(lightVec + eyeVec);

    // calculate diffuse component
    vec3 diffuse = vec3(max(0.5*dot(NormalVec, lightVec), 0.5)) * diffuseMaterial;

    // calculate specular component
    vec3 specular = vec3(max(dot(NormalVec, halfVec), 0.0));
    specular = pow(specular.x, 32.0) * specularMaterial;

    // combine diffuse and specular contributions and output final vertex color
//    gl_FragColor.rgb = diffuse + specular;
    vec4 col = texture2D(u_Texture, vert_TexCoord);
    vec4 colMix = col * (diffuse+0.1) + (specular*0.9);
//      gl_FragColor = vec4(col, 1.0);
    outColor = colMix;
}
