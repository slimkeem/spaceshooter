in vec3 vert_TexCoords;

out vec4 out_Color;

uniform samplerCube u_SkyBox;

void main(void)
{
  out_Color = texture(u_SkyBox, vert_TexCoords);
  out_Color.w = 0;
}
