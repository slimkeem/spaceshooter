layout(location=0) in vec3 position;
layout(location=1) in vec2 texCoord;
layout(location=2) in vec3 normal;
layout(location=3) in vec3 color;

out vec3 vert_Normal;
out vec3 vert_Incident;

uniform vec4 camPos;

void main(void)
{
//  vert_Normal      = normalize(gl_NormalMatrix * gl_Normal);
  vert_Normal = normal;
  vec4 in_Position = vec4(position, 1.0);
  vert_Incident = normalize(in_Position.xyz - camPos.xyz);
  gl_Position = gl_ModelViewProjectionMatrix * in_Position;
}
