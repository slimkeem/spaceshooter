in vec2 vert_TexCoord;

out vec4 outColor;

uniform sampler2D u_Texture;

void main(void)
{
  outColor = texture2D(u_Texture, vert_TexCoord);
}
