in vec3 var_Color;
in vec3 var_TexCoord;

out vec4 out_Color;

uniform sampler2D u_Tex;

void main(void)
{
  out_Color = vec4(var_Color, 1.0) /* * texture(u_Tex, var_TexCoord)*/;
}
