layout(location=0) in vec3 position;
layout(location=1) in vec2 texCoord;
layout(location=2) in vec3 normal;
layout(location=3) in vec3 color;

out  vec2 vert_TexCoord;
out  vec3 vert_Normal;

void main(void)
{
  gl_Position     = gl_ModelViewProjectionMatrix * vec4(position, 1.0);
  vert_TexCoord   = texCoord;
  vert_Normal      = normalize(gl_NormalMatrix * gl_Normal);
}
