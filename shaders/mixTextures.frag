in  vec2 var_TexCoord;

out vec4 outColor;

uniform sampler2D u_Tex0;
uniform sampler2D u_Tex1;
uniform float u_Time;

void main(void)
{
  vec4 colTex0 = texture2D(u_Tex0, var_TexCoord);
  vec4 colTex1 = texture2D(u_Tex1, var_TexCoord);
  float factor = (sin(u_Time * 3.0) + 1.0) / 2.0;

  outColor = mix(colTex0, colTex1, factor);
}
