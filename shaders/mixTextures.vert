layout(location=0) in vec3 position;
layout(location=1) in vec2 texCoord;
layout(location=2) in vec3 normal;
layout(location=3) in vec3 color;
layout(location=4) in vec3 transformations;

out  vec2 var_TexCoord;

void main(void)
{
  gl_Position     = gl_ModelViewProjectionMatrix * vec4(position.x + transformations.x, position.yz, 1.0);
  var_TexCoord    = texCoord;
}
