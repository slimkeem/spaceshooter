in vec3 vert_Normal;
in vec3 vert_Incident;

out vec4 out_Color;

uniform samplerCube cubeMap;

void main(void)
{
  vec3 normalVec = normalize(vert_Normal);
  vec3 incidentVec = normalize(vert_Incident);
  float ratio = 1.00 / 1.52;
  // Calculate reflection color
  vec3 reflectColor = textureCube(cubeMap, reflect(incidentVec, normalVec)).xyz;
  
  // Fragment color
  out_Color = vec4(reflectColor, 1.0);
}
