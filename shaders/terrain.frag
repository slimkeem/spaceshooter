in vec2 var_TexCoord;

out vec4 out_Color;

uniform sampler2D u_colorMap;
uniform sampler2D u_lightMap;
uniform sampler2D u_normalMap;

void main()
{
  vec4 normal = texture2D(u_normalMap, var_TexCoord);
  normal = normalize(normal * 2.0 - 1.0);

  vec4 colorTex = texture2D(u_colorMap, var_TexCoord);
  vec4 lightTex = texture2D(u_lightMap, var_TexCoord);

  out_Color = colorTex*lightTex;
  out_Color = vec4(colorTex*lightTex/* * normal*/);
//  out_Color = vec4(colorTex*lightTex* normal);
//  out_Color = mix(colorTex, lightTex, 0.0f);
}
