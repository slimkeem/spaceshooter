layout(location=0) in vec4 position;

out  vec2 vert_TexCoords;

void main(void)
{
  gl_Position     = gl_ModelViewProjectionMatrix * vec4(position.xy, 0.0, 1.0);
  vert_TexCoords  = position.zw;
}
