in vec3 colorVF;
in vec2 texCoordVF;

out vec4 outColor;

//uniform vec4 u_Color;
uniform sampler2D u_Tex0;
uniform sampler2D u_Tex1;
uniform float u_time;

void main(void)
{
  vec4 colTex0 = texture2D(u_Tex0, texCoordVF);
  vec4 colTex1 = texture2D(u_Tex1, texCoordVF);
  float factor = (sin(u_time * 3.0) + 1.0) / 2.0;

  outColor = mix(colTex0, colTex1, factor);
//  outColor = texture2D(u_Tex0, texCoordVF) * vec4(colorVF, 1.0);
//  outColor = vec4(colorVF, 1.0);
//  outColor = u_Color;
//  gl_FragColor = u_Color;
//  gl_FragColor = texture2D(u_Tex, texCoordVF);
}
