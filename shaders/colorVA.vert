layout(location=0) in vec3 position;
layout(location=1) in vec2 texCoord;
layout(location=2) in vec3 normal;
layout(location=3) in vec3 color;

out vec3 var_Color;
out vec2 var_TexCoord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

void main(void)
{
  gl_Position  = gl_ModelViewProjectionMatrix * vec4(position,1.0);
//  gl_Position = proj * view * model * vec4(position, 1.0);
  var_Color    = color;
  var_TexCoord = texCoord;
}
