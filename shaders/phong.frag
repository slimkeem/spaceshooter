in vec2 vert_TexCoord;
in  vec3 vert_Normal;

out vec4 outColor;

uniform sampler2D u_Texture;

void main(void)
{
  vec3 lightVec = vec3(1.0f, 1.0f, 1.0f);
  vec3 viewVec = vec3(0.0, 0.0, 1.0);

  vec3 nVec = normalize(vert_Normal);
	vec3 lVec = normalize(lightVec);
	vec3 vVec = normalize(viewVec);
	
	float diffuse = clamp(dot(lVec, nVec), 0.0, 1.0);
	float specular = pow(clamp(dot(reflect(-vVec, nVec), lVec), 0.0, 1.0), 32.0);
	
  outColor = texture2D(u_Texture, vert_TexCoord) * (diffuse+0.1) + (specular*0.9);
}
