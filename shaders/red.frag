in vec2 var_TexCoord;

out vec4 out_Color;

uniform sampler2D u_colorMap;

void main()
{
        out_Color = texture2D(u_colorMap, var_TexCoord);
//  gl_FragColor = vec4(0.0f, 1.0f, 1.0f, 1.0f);
}
