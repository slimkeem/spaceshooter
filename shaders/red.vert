layout(location=0) in vec3 position;
layout(location=1) in vec2 texCoord;

  uniform sampler2D u_heightMap;

  out vec2 var_TexCoord;

  void main()
  {
      vec4 position = vec4(gl_Vertex);
//      gl_TexCoord[0] = gl_MultiTexCoord0;
//      position.y = texture2D(u_heightMap, gl_TexCoord[0].xy).x * 600.0, - 600.0;
      gl_Position = gl_ModelViewProjectionMatrix * position;
      var_TexCoord = texCoord;
  }
