varying vec2 textCoord;


void main(void)
{
    vec4 _position = gl_Vertex;
    textCoord = gl_MultiTexCoord0.xy;
    gl_Position = gl_ModelViewProjectionMatrix * _position;
}
