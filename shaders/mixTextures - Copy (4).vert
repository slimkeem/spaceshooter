in vec3 position;
in vec2 texCoord;
in vec3 color;

out vec3 colorVF;
out vec2 texCoordVF;

void main(void)
{
  gl_Position     = gl_ModelViewProjectionMatrix * vec4(position, 1.0);
  colorVF = color;
  texCoordVF = texCoord;
}
