in vec2 vert_TexCoords;

out vec4 outColor;

uniform sampler2D u_Text;
uniform vec3 u_TextColor;

void main(void)
{
  vec4 sampled = vec4(1.0, 1.0, 1.0, texture(u_Text, vert_TexCoords).r);
  outColor = vec4(u_TextColor, 1.0) * sampled;
}
