layout(location=0) in vec3 position;
layout(location=1) in vec2 texCoord;
layout(location=2) in vec3 normal;
layout(location=3) in vec3 color;

out vec3 var_Color;
out vec2 var_TexCoord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

uniform sampler2D u_heightMap;

void main(void)
{
  vec4 in_position   = vec4(position,1.0);
  gl_TexCoord[0]  = gl_MultiTexCoord0;
  position.y   = texture2D(u_heightMap, gl_TexCoord[0].xy).x * 600.0 - 600.0;
  gl_Position  = gl_ModelViewProjectionMatrix * in_position;
}
