#include <GL/glew.h>
#include <GL/wglew.h>
#include <windows.h>

#include <iostream>


#include <GL/freeglut.h>
#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include "../include/Input.hpp"
#include "../include/FpsCounter.hpp"
#include "../include/GameManager.hpp"
#include <glm/glm.hpp>
#include <SOIL/SOIL.h>


std::shared_ptr<GameManager> gm;
siut::FpsCounter counter;


int window;
unsigned int windowWidth = 700, windowHeight = 700;
bool keyPressed[30];
bool mouseMotion = false;
bool firstMouse = true;
int mousePosX = 400, mousePosY = 300;
float moveX, moveY;
float aimX = 0, aimY = 0;

void init()
{
  glClearColor(0.0, 0.0, 0.0, 0.0);
  glShadeModel(GL_SMOOTH);
  glEnable(GL_DEPTH_TEST);

  GLenum error = glewInit(); // Enable GLEW
  if (error != GLEW_OK) {
    // If GLEW fails
    std::cout << "Error: %s\n" << glewGetErrorString(error) << std::endl;
//    return 0;
  }


  counter.start();

  gm.reset(new GameManager());
  gm->init();

  for(int i=0; i<30; i++)
    keyPressed[i]=false;

  //set mouse at center of screen

  glutSetCursor(GLUT_CURSOR_CROSSHAIR);
}

void display()
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  gm->update(counter.fps());
  gm->render();

//  if(keyPressed[KEY_ID_W]==true)      gm->getCam()->moveForward();
//  if(keyPressed[KEY_ID_S]==true)      gm->getCam()->moveBackward();
//  if(keyPressed[KEY_ID_SPACE]==true)  gm->getCam()->moveUp();
//  if(keyPressed[KEY_ID_C]==true)      gm->getCam()->moveDown();

  //////////////////////////
  if(keyPressed[KEY_ID_A]==true)      gm->getSpaceShip()->moveLeft();
  if(keyPressed[KEY_ID_D]==true)      gm->getSpaceShip()->moveRight();
  if(mouseMotion==true)               gm->getSpaceShip()->spin(aimX);
//  if(mouseMotion==true)               gm->getBullet()->setDir(glm::vec3(0.0,0.0,-1.1f));
  if(keyPressed[MOUSE_LEFT_BUTTON_DOWN]==true) gm->playerShoot();
  if(keyPressed[MOUSE_RIGHT_BUTTON_DOWN]==true) gm->toggleWeapon();

  /////////////////////////

  glutSwapBuffers();
  glutPostRedisplay();

}

////////////////////////
void spinDisplay(void)
{

//   glutPostRedisplay();
}
///////////////////////////



void keyDown(unsigned char key, int x, int y)
{
  switch (key)
  {
    case 'q':
    case 27:
      glutDestroyWindow(window);
#ifndef _WIN32
      // Must use this with regular glut, since it never returns control to main().
      exit(0);
#endif
      break;
      
    case 'w':
      keyPressed[KEY_ID_W] = true;
      break;
    case 'a':
      keyPressed[KEY_ID_A] = true;
      break;
    case 's':
      keyPressed[KEY_ID_S] = true;
      break;
    case 'd':
      keyPressed[KEY_ID_D] = true;
      break;
    case ' ':
      keyPressed[KEY_ID_SPACE] = true;
      break;
    case 'c':
      keyPressed[KEY_ID_C] = true;
      break;

    default:
      glutPostRedisplay();
  }
}

void keyUp(unsigned char key, int x, int y)
{
  switch (key)
  {
    case 'w':
      keyPressed[KEY_ID_W] = false;
      break;
    case 'a':
      keyPressed[KEY_ID_A] = false;
      break;
    case 's':
      keyPressed[KEY_ID_S] = false;
      break;
    case 'd':
      keyPressed[KEY_ID_D] = false;
      break;
    case ' ':
      keyPressed[KEY_ID_SPACE] = false;
      break;
    case 'c':
      keyPressed[KEY_ID_C] = false;
      break;

  }
}

void mousePressed(int button, int state, int posX, int posY)
{
  if(button==GLUT_LEFT_BUTTON && state==GLUT_DOWN)
  {
//    mousePosX = posX;
//    mousePosY = posY;
    keyPressed[MOUSE_LEFT_BUTTON_DOWN] = true;
  }  
  if(button==GLUT_LEFT_BUTTON && state==GLUT_UP)
    keyPressed[MOUSE_LEFT_BUTTON_DOWN] = false;

  ///////////////////////////////////////////////////
  if(button==GLUT_MIDDLE_BUTTON && state==GLUT_DOWN){
    keyPressed[MOUSE_MIDDLE_BUTTON_DOWN] = true;
//    glutIdleFunc(spinDisplay);
  }
  if(button==GLUT_MIDDLE_BUTTON && state==GLUT_UP){
    keyPressed[MOUSE_MIDDLE_BUTTON_DOWN] = false;
  }

  if(button==GLUT_RIGHT_BUTTON && state==GLUT_DOWN){
    keyPressed[MOUSE_RIGHT_BUTTON_DOWN] = true;
//    glutIdleFunc(spinDisplay);
  }
  if(button==GLUT_RIGHT_BUTTON && state==GLUT_UP){
    keyPressed[MOUSE_RIGHT_BUTTON_DOWN] = false;
  }
  ///////////////////////////////////////////////////
}

void mouseMoved(int posX, int posY)
{
  if(firstMouse) // this bool variable is initially set to true
  {
    glutWarpPointer(windowWidth / 2, windowHeight / 2);
    posX = windowWidth / 2;
    posY = windowHeight / 2;
    mousePosX = posX;
    mousePosY = posY;
    firstMouse = false;
    mouseMotion = true;
  }

  float diffX = posX - mousePosX;
  float diffY = posY - mousePosY;
  mousePosX = posX;
  mousePosY = posY;

  // Implement quaternion based mouse move
  float sensitivity = 0.05f;
  diffX *= sensitivity;
  diffY *= sensitivity;

  aimX += diffX;
  aimY += diffY;

}

void reshape(int w, int h)
{
  glViewport(0, 0, (GLsizei) w, (GLsizei) h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
//  glOrtho(-50, 700, -50, 700, -50, 50);
  gluPerspective(60.0f, float(w)/float(h) ,1.0f, 3000.0f);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
//  gluLookAt(0.0, 0.0, 10.0,     0.0, 0.0, 0.0,    0.0, 1.0, 0.0);
}

int main(int argc, char** argv)
{
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH | GLUT_RGB);
  glutInitWindowSize(windowWidth, windowHeight);
  glutInitWindowPosition(10, 10);
  window = glutCreateWindow("Space Shooter 3D");


  init();
  glutKeyboardFunc(keyDown);
  glutKeyboardUpFunc(keyUp);
  glutReshapeFunc(reshape);
  glutDisplayFunc(display);
  glutMouseFunc(mousePressed);
  glutPassiveMotionFunc(mouseMoved);
  glutMotionFunc(mouseMoved);

  // Add other callback functions here..

  glutMainLoop();
  return 0;
}
